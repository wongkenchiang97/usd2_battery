#include <stdio.h>
#include <modbus/modbus.h>
#include <ros/ros.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "hello_node");
    modbus_t *mb;
    uint16_t tab_reg[32];

    mb = modbus_new_rtu("/dev/battery_essiot", 9600, 'N', 8, 1);
    modbus_set_slave(mb , 0);
    modbus_connect(mb);
    
    while (ros::ok())
    {
        int a = modbus_read_registers(mb, 0, 1, tab_reg);
        if (a < 0)
        {
            std::runtime_error("failed to read registers.");
        }
        else
        {
            std::cout << "a: " << a << std::endl;
        }
    }

    modbus_close(mb);
    modbus_free(mb);

    return 0;
}